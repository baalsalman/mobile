import 'dart:io';

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:mena_w_fina/model/foods.dart';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;


Future<List<Photo>> fetchPhotos(  ) async {
  final response =
  await http.get('http://192.168.8.118:8080/api/foods',headers: {HttpHeaders.authorizationHeader: "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU2OTkyMDk3OX0.oXMR7lkKblrom3mZw2OjpqU65cIIV0QeiblZTwv7JCISTQakLxz7VBJRXj-wKtgn-eIqOGI4uwceoVu1MouLiw"});
  // Use the compute function to run parsePhotos in a separate isolate.
  return compute(parsePhotos, response.body);
}

// A function that converts a response body into a List<Photo>.
List<Photo> parsePhotos(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<Photo>((json) => Photo.fromJson(json)).toList();
}
