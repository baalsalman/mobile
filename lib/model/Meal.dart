class Meal {
  int id;
  String name;
  String price;
  String imagePath;
  String itemKey;

  Meal(this.id, this.name, this.price, this.imagePath, this.itemKey) {}
}
