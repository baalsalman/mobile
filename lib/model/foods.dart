class Photo {
  final String albumId;
  final String id;
  final String title;
  final String url;
  final String thumbnailUrl;

  Photo({this.albumId, this.id, this.title, this.url, this.thumbnailUrl});

  factory Photo.fromJson(Map<String, dynamic> json) {
    return Photo(
      albumId: json['description'] as String,
      id: json['id'] as String,
      title: json['description'] as String,
      url: json['foodType'] as String,
      thumbnailUrl: json['description'] as String,
    );
  }
}