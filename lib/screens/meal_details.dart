import 'package:flutter/material.dart';
import 'package:mena_w_fina/model/Meal.dart';
import 'package:mena_w_fina/widgets/image_banner.dart';

class MealDetail extends StatelessWidget {
  final int _mealID;

  MealDetail(this._mealID);

  @override
  Widget build(BuildContext context) {
    final meal =
        Meal(1, "name", "nnnnn", 'assets/images/kiyomizu-dera.jpg', "bash");

    return Scaffold(
        appBar: AppBar(
          title: Text(meal.name),
        ),
        backgroundColor: Color.fromRGBO(255, 255, 255, .9),
        body: SingleChildScrollView(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                ImageBanner(assetPath: meal.imagePath),
                Card(
                    child: Container(
                  padding: EdgeInsets.all(3.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      ChoiceChip(label: Text('Item'), selected: false),
                      ChoiceChip(label: Text('Item'), selected: false),
                      ChoiceChip(label: Text('Item'), selected: false),
                      ChoiceChip(label: Text('Item'), selected: false)
                    ],
                  ),
                )),
                Row(
                  children: <Widget>[
                    Chip(
                      avatar: CircleAvatar(
                          backgroundColor: Colors.blue.shade900,
                          child: Text('ML')),
                      label: Text('Lafayette'),
                    ),
                    Chip(
                      avatar: CircleAvatar(
                        backgroundColor: Colors.grey.shade800,
                        child: Text('AB'),
                      ),
                      label: Text('Aaron Burr'),
                    ),
                    InputChip(
                        avatar: CircleAvatar(
                          backgroundColor: Colors.grey.shade800,
                          child: Text('AB'),
                        ),
                        label: Text('Aaron Burr'),
                        onPressed: () {
                          print('I am the one thing in life.');
                        }),
                    ChoiceChip(label: Text('Item'), selected: true)
                  ],
                ),
                Card(
                  child: Container(
                    height: 300,
                    margin: EdgeInsets.fromLTRB(0, 16.0, 0, 0),
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text("Hello Bashayer"),
                        Text("Hello Bashayer Next Time"),
                      ],
                    ),
                  ),
                ),
                Card(
                  child: Container(
                    height: 100,
                    margin: EdgeInsets.fromLTRB(0, 16.0, 0, 0),
                    padding: EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text("Hello Bashayer"),
                        Text("Hello Bashayer Next Time"),
                      ],
                    ),
                  ),
                ),
                RaisedButton(
                  child: Text("BUY"),
                  disabledColor: Colors.blue,
                  disabledTextColor: Colors.white,
                  textColor: Colors.white70,
                  color: Colors.indigo,
                  onPressed: _onBuyMeal(),
                )
              ]),
        ));
  }

  _onBuyMeal() {}
}
