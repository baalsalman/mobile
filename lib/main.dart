import 'package:flutter/material.dart';
import 'package:mena_w_fina/screens/meal_details.dart';
import 'package:ribbon/ribbon.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'model/foods.dart';
import 'widgets/photos_list.dart';
import 'connection/API.dart';

import 'package:flutter/foundation.dart';
 import 'package:http/http.dart' as http;
import 'model/Meal.dart';
import 'style.dart';
import 'widgets/image_banner.dart';
import 'widgets/text_section.dart';

void main() => runApp(MyApp());

const MenuRoute = '/';
const MealDetailsRoute = '/meal_detail';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final Future<Photo> Photos;
  MyApp({Key key, this.Photos}) : super(key: key);



  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Mena W Fina',
      onGenerateRoute: _routes(),
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Todays Menu'),
    );
  }

  ThemeData _theme() {
    return ThemeData(
        appBarTheme: AppBarTheme(textTheme: TextTheme(title: AppBarTextStyle)),
        textTheme: TextTheme(
          title: TitleTextStyle,
          subtitle: SubTitleTextStyle,
          caption: CaptionTextStyle,
          body1: Body1TextStyle,
        ));
  }

  RouteFactory _routes() {
    return (settings) {
      final Map<String, dynamic> arguments = settings.arguments;
      Widget screen;
      switch (settings.name) {
        case MenuRoute:
          screen = MyHomePage();
          break;
        case MealDetailsRoute:
          screen = MealDetail(arguments['id']);
          break;
        default:
          return null;
      }
      return MaterialPageRoute(builder: (BuildContext context) => screen);
    };
  }
}


class MyHomePage extends StatefulWidget {

  MyHomePage({Key key, this.title}) : super(key: key);



  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}



class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;





  Future<List<Photo>> post;

  @override
  void initState() {
    super.initState();
    post = fetchPhotos() ;
  }

  TextEditingController editingController = TextEditingController();

  final List<Meal> strings = [
    Meal(1, "name", "16 SR", 'assets/images/kiyomizu-dera.jpg', "SH-d123"),
    Meal(1, "name", "11 SR", 'assets/images/kiyomizu-dera.jpg', "bash"),
    Meal(1, "name", "5 SR", 'assets/images/kiyomizu-dera.jpg', "bash"),
    Meal(1, "name", "18 SR", 'assets/images/kiyomizu-dera.jpg', "bash"),
    Meal(1, "name", "1 SR", "", "bash")
  ];

  final List<Meal> finalStrings = [
    Meal(1, "name", "16 SR", 'assets/images/kiyomizu-dera.jpg', "SH-123"),
    Meal(1, "name", "11 SR", 'assets/images/kiyomizu-dera.jpg', "bash"),
    Meal(1, "name", "5 SR", 'assets/images/kiyomizu-dera.jpg', "bash"),
    Meal(1, "name", "180 SR", 'assets/images/kiyomizu-dera.jpg', "bash"),
    Meal(1, "name", "1 SR", "", "bash")
  ];


   @override
  Widget build(BuildContext context) {




           return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      backgroundColor: Colors.white,

      body: Column(
        children: <Widget>[
          Card(
              elevation: 10,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              margin: EdgeInsets.all(8.0),
              child: TextField(
                controller: editingController,
                onChanged: (value) {
                  filterSearchResults(value);
                },
                decoration: InputDecoration(
                    hintText: "Search", prefixIcon: Icon(Icons.search)),
              )),
          Expanded(
              child: ListView.builder(
            itemCount: strings.length,
            itemBuilder: (context, index) =>
                getListOfFood(context,strings[index] ),
          ))
        ],
      ),
//      floatingActionButton: FloatingActionButton(
//        onPressed: _onNewMealTap(),
//        tooltip: 'Increment',
//        child: Icon(Icons.camera_alt),
//      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }






  void filterSearchResults(String query) {
    if (query.isNotEmpty) {
      List<Meal> searchResult = List<Meal>();
      finalStrings.forEach((item) {
        if (item.name.contains(query)) { //todo search by the key as well
          searchResult.add(item);
        }
      });

      setState(() {
        strings.clear();
        strings.addAll(searchResult);
      });
      return;
    } else {
      setState(() {
        strings.clear();
        strings.addAll(finalStrings);
      });
    }
  }

  _onMealTap(BuildContext context, int mealId) {
    Navigator.pushNamed(context, MealDetailsRoute, arguments: {"id": mealId});
  }

  Widget getListOfFood(BuildContext context, Meal meal) {




    return GestureDetector(
      onTap: () => _onMealTap(context, meal.id),
      key: Key('meal_list_item_${meal.id}'),
      child: FutureBuilder<List<Photo>>(
        future: fetchPhotos(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);

          return snapshot.hasData
              ? PhotosList(photos: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),

    );
  }

  _onNewMealTap() {
    //todo
  }
}



