import 'package:flutter/material.dart';

class TextSection extends StatelessWidget {
  final String _text;
  final String _text2;

  TextSection(this._text, this._text2);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Color.fromRGBO(255, 255, 255, 1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(16.0),
            child: Text(_text2),
          ),
          Container(
            padding: EdgeInsets.all(16.0),
            child: Text(_text),
          )
        ],
      ),
    );
  }
}
