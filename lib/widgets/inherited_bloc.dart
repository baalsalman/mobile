import 'package:flutter/cupertino.dart';

class InheritedBlocs extends InheritedWidget {
  InheritedBlocs(
      {Key key,
      this.name,
      //this.searchBloc,
      this.child})
      : super(key: key, child: child);

  final Widget child;

  //final SearchBloc searchBloc;
  final String name;

  static InheritedBlocs of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(InheritedBlocs)
        as InheritedBlocs);
  }

  @override
  bool updateShouldNotify(InheritedBlocs oldWidget) {
    return true;
  }
}
