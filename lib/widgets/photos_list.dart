


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ribbon/ribbon.dart';

import 'text_section.dart';
import 'package:mena_w_fina/model/foods.dart';
class PhotosList extends StatelessWidget {
  final List<Photo> photos;
  var size ;


  PhotosList({Key key, this.photos,this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {


    return ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: List.generate(  photos.length, (index) {
          return Center(
            child: Card(
              elevation: 10,
              shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
              margin: EdgeInsets.all(8.0),
              child: Stack(
                children: [

                  Ribbon(
                      titleStyle: TextStyle(
                          color: Colors.greenAccent,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                      nearLength: 80,
                      farLength: 100,
                      title: 'New!',
                      location: RibbonLocation.topStart,
                      child: Image.network(photos[index].title, height: 245.0)),

                  Positioned(
                    width: MediaQuery.of(context).size.width,
                    bottom: -5,
                    left: -5,
                    child: TextSection(photos[index].id, photos[index].url),
                  )
                ],
              ),
            ),


          );
        }
        )
    );


  }

}
